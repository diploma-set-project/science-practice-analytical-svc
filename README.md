# science-practice-analytical-svc

The analytical service for Azure Computer Vision recognition experiment

```bash
python3 -m pip install -U numpy pymongo
python3 -m pip install 'pymongo[srv]'

python3 -m pip install git+https://github.com/mongodb/bson-numpy.git

python3 analytic.py
```